def foo():
    print('hello world')

def foo2(x, y):
    print(x + y)

def foo3(a, b) -> int:
    return a * b

a = lambda x: x ** 2
print( (lambda x: x ** 2)(3) )

CONSTANTA = 123