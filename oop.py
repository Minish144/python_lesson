class Class_Name:
    def __init__(self, x=None):
        self.x = x
        self.y = 123

    def get_x(self):
        print(self.x)
        
    def __get_y(self):
        print(self.y)
    
    def get_y_public(self):
        self.__get_y()

var = Class_Name(123)
var.get_y_public()
