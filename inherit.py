class Gun:
    def __init__(self):
        pass

    def shoot(self):
        print('trrrrrr')
    
class SMG(Gun):
    def triple_shot(self):
        print('trr trr trr')

def main():
    smg = SMG()
    smg.shoot()

if __name__ == "__main__":
    main()